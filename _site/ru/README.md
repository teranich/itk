###Проект сделан с помощью статического генератора сайтов Jekyll

https://jekyllrb.com/


####Запустить проект локально

`jekyll serve`

####Сбилдить проект

`jekyll build`

В результате, в папке _site будет создана статическая версия сайта
готовая к загрузке на хостинг  

####Структура


* _data/    //файлы с данными на всех языках
* _includes //секци сайта 
* _layouts  //верстка страниц
* css fonts images js //стили, шрифты, картинки, скрипты
* _config.yaml //глобальные настройки приложения
* index.html //главная страница
* branch-pipes.md, flanges.md .. //станицы разделов
